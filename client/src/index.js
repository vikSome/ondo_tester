import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import * as serviceWorker from './serviceWorker';
import AppRouter from './routers/AppRouter';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme ({
  palette: {
      primary: { 
      main: '#494949',
  },
  secondary: { 
      main: '#8ABB2A'
  },
  text: {
    primary: "#494949",
    secondary: "#494949"
  },
  },
  overrides:{
    MuiSlider: {
      thumb:{
      color: "#8ABB2A",
      },
      track: {
        color: '#494949',
        height: 4,
      },
      rail: {
        color: '#494949',
        height: 3,
      },
      mark: {
        height: 4,
        width: 4,
        borderRadius: '4px'
      }
    }
}
})

ReactDOM.render(
  <MuiThemeProvider theme={theme}>  
   <React.StrictMode>
    <AppRouter />
  </React.StrictMode>
  </MuiThemeProvider>
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
