import React from 'react';
import Button from '@material-ui/core/Button';

function ButtonElement() {
    return (
      <Button variant="contained" color="primary">
        Hello World
      </Button>
    );
  }

  export default ButtonElement;