import React from "react";
import socketIOClient from "socket.io-client";
import PlcComponent from './PlcComponent';

const ENDPOINT = "http://127.0.0.1:5000/";

 const AppDashboard = (props) => {

 const socket = socketIOClient(ENDPOINT);

  return (  
      <div className="App">
      <div className = "plc-components">
      <PlcComponent socket={socket} />
      </div>
    </div>
   
  );
  }

export default AppDashboard;

