import React, { useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  Paper,
  Switch,
  Input,
  TextField,
  Divider,
  ThemeProvider,
} from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { ReactComponent as HumidityIcon } from "../../assets/humidityIcon.svg";
import { ReactComponent as TemperatureIcon } from "../../assets/temperatureIcon.svg";
import ChartOne from "./chartOne";
import ChartTwo from "./chartTwo";
import Slider from "@material-ui/core/Slider";
import Marks from "../utils/sliderMarks";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    minHeight: "89vh",
  },
  main: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(2),
    maxWidth: "200em",
  },
  select: {
    width: 150,
  },
  label: {
    fontSize: 16,
    fontWeight: 600,
    width: 100,
    marginBottom: 15,
    color: theme.palette.primary,
  },
  button: {
    width: "auto",
    padding: 10,
    marginBottom: 20,
    color: "white",
  },
  widget: {
    width: "40em",
    padding: "10px 10px",
    backgroundColor: "#FFFFFF",
    height: "auto",
    margin: theme.spacing(4),
  },
  switch: {
    // display: 'block'
  },
  widgetInner: {
    fontSize: "16px",
    display: "inline-flex",
    justifyContent: "space-between",
    width: "100%",
    padding: "16px 16px 0px 16px",
  },
  widgetInnerLabel: {
    paddingTop: "5px",
    fontSize: "16px",
  },
  widgetInnerWrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "0px",
  },
  widgetInnerInput: {
    display: "block",
  },
  mainTwo: {
    display: "flex",
    justifyContent: "flex-start",
    flexDirection: "column",
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(2),
    maxWidth: "200em",
    backgroundColor: "#F5F5F5",
  },
  widgetBit: {
    display: "inline-flex",
    width: "800px",
    padding: "20px 10px",
    backgroundColor: "#F5F5F5",
    height: "auto",
    margin: theme.spacing(6),
    marginTop: theme.spacing(2),
    border: "2px solid grey",
  },
  BitIcon: {
    fontSize: "45px",
  },
  bitLabel: {
    fontWeight: "500",
  },
  widgetBitTitle: {
    display: "inline-flex",
    marginLeft: theme.spacing(4),
  },
  ventActionOn: {
    color: "#37966F",
    marginLeft: "5px",
    padding: "16px 16px 0px 0px",
    fontSize: "16px",
  },
  ventActionOff: {
    color: "#d32f2f",
    marginLeft: "5px",
    padding: "16px 16px 0px 0px",
    fontSize: "16px",
  },
  title: {
    width: "100%",
    padding: "16px",
    display: "inline-flex",
    alignItems: "center",
  },
  regIcon: {
    marginRight: "5px",
  },
  divider: {
    width: "100%",
    height: "2px",
    backgroundColor: "#8ABB2A",
  },
  widgetInputWrapper: {
    display: "inline-flex",
    padding: "16px 16px 19px 16px",
  },
  widgetInputLabel: {
    paddingTop: "5px",
    fontSize: "16px",
    width: "80%",
    textAlign: "left",
  },
  widgetInput: {
    width: "20%",
    padding: "0px 2px",
    border: "1px solid #707070;",
    borderRadius: "4px",
  },
  widgetInnerVent: {
    padding: "0px",
    display: "inline-flex",
    justifyContent: "space-between",
    width: "100%",
  },
  widgetInnerChart: {
    display: "inline-flex",
    justifyContent: "center",
    width: "100%",
    padding: "16px 16px 0px 16px",
  },
  widgetInnerVentRow: {
    display: "block",
    width: "100%",
    padding: "0px 0px 19px 0px",
  },
  ventSwitch: {
    color: "#37966F",
    marginLeft: "5px",
    padding: "0px 5px 0px 5px",
    fontSize: "16px",
    border : "1px solid #37966F",
    borderRadius: "4px"
  },
  widgetSlider: {
    padding: "0px 2px",
    marginTop: "30px",
  },
  widgetSliderWrapper: {
    display: "inline-flex",
    width: "100%",
    padding: "16px 0px 19px 0px",
  },
  widgetInputLabel: {
    paddingTop: "5px",
    fontSize: "16px",
    width: "100%",
    textAlign: "left",
    marginBottom: "10px",
    paddingLeft: "15px"
  },
  widgetVent: {
    width: 400,
    padding: "10px 10px",
    backgroundColor: "#FFFFFF",
    minHeight: "47vh",
    margin: theme.spacing(4),
  },
  widgetInnerSlider: {
    width: '100%',
    display: 'inline-flex'
  },
  widgetMeasureSliderWrapper: {
    display: "block",
    width: "100%",
    padding: "16px 5px 30px 5px",
  },
  sliderButton: {
    transform: 'scale(0.45)',
    backgroundColor: '#707070',
    position: 'relative',
    top: '5px',
    '&:hover': {
      backgroundColor: '#494949' /* Green */,
      opacity: 0.7

    }
  },
  sliderButtonIcon: {
    fill: '#ffffff',
  },
  ventActionOnBox: {
    border : "1px solid #37966F",
    borderRadius: "4px",
    padding: "3px 6px 3px 6px"
  },
  ventActionOffBox: {
    border : "1px solid #d32f2f",
    borderRadius: "4px",
    padding: "3px 6px 3px 6px"
  }
}));


const VentTwoWidget = (props) => {

  const capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  const handleDecrement = () => {
    let newValue = value - 1
    setValue(newValue)
    props.handleInput(null, newValue)
  }


  const handleIncrement = () => {
    let newValue = value + 1
    setValue(newValue)
    props.handleInput(null, newValue)
  }

  const [value, setValue] = React.useState(props.openTime);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  // const setSliderValue = (openTime) => {
  //   setValue(value)
  // }

  // useEffect(() => {
  //   if(props.openTime) {
  //   setSliderValue(props.openTime)
  // } else {
  //   setSliderValue(0)
  // }
  // })


  useEffect(() => {
    setValue(props.openTime)
  },[props.openTime])

  const classes = useStyles();

  return (

    <div>


      <Paper
        xs={3}
        className={classes.widgetVent}
        elevation={3}
      >
        <div className={classes.widgetInnerWrapper}>
          <div className={classes.title}>
            <Typography variant="h6">{capitalize(props.stateName)}</Typography>
          </div>
          <Divider
            className={classes.divider}
            orientation="vertical"
            variant="fullWidth"
          />

          <div className={classes.widgetInnerChart}>


            <ChartTwo
              ventTwoTimeOpen={props.ventTwoTimeOpen}
              openTime={props.openTime}
              stateName={props.stateName}
            />

          </div>

          <label className={classes.widgetInputLabel} name="manualReg">
            Open Time Vent 2(s)
          </label>
          <div className={classes.widgetSliderWrapper}>
            <IconButton onClick={handleDecrement} className={classes.sliderButton} color="primary" aria-label="upload picture" component="span">
              <RemoveIcon className={classes.sliderButtonIcon} />
            </IconButton>

            <Slider

              className={classes.widgetSlider}
              onChange={handleChange}
              onChangeCommitted={props.handleInput}
              valueLabelDisplay="auto"
              value={value}
              step={1}
              min={0}
              max={600}
              marks={Marks.ventOpenTIme}
            ></Slider>
            <IconButton onClick={handleIncrement} className={classes.sliderButton} aria-label="upload picture" component="span">
              <AddIcon className={classes.sliderButtonIcon} />
            </IconButton>
          </div>


          <div className={classes.widgetInnerVent}>
            <Typography variant="h6" className={classes.widgetInner}>
              Action:{" "}
            </Typography>

            {props.opening && (
              <Typography className={classes.ventActionOn} >
              <span className = {classes.ventActionOnBox}>  Opening </span>
              </Typography>
            )}
            {props.opening && (
              <Typography className={classes.ventActionOff} >
              <span className = {classes.ventActionOffBox}>  Closing </span>
              </Typography>
              )}
          </div>



          <div className={classes.widgetInnerVentRow}>
            <Typography variant="h6" className={classes.widgetInner}>
              Open Switch:{" "}
              {props.openSwitch && (
                <Typography className={classes.ventSwitch} >
                  Reached
                </Typography>
              )}
            </Typography>

            <Typography variant="h6" className={classes.widgetInner}>
              Close Switch:{" "}
              {props.closeSwitch && (
                <Typography className={classes.ventSwitch} >
                  Reached
                </Typography>
              )}
            </Typography>
          </div>

        </div>
      </Paper>
    </div>
  );
};

export default VentTwoWidget;

