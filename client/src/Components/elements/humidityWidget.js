import React, {useEffect} from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  Paper,
  Switch,
  Input,
  TextField,
  Divider,
  ThemeProvider,
} from "@material-ui/core/";
import Button from "@material-ui/core/Button";
import { ReactComponent as HumidityIcon } from "../../assets/humidityIcon.svg";
import { ReactComponent as TemperatureIcon } from "../../assets/temperatureIcon.svg";
import ChartOne from "./chartOne";
import ChartTwo from "./chartTwo";
import Slider from "@material-ui/core/Slider";
import Marks from "../utils/sliderMarks";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";

const useStyles = makeStyles((theme) => ({

  select: {
    width: 150,
  },
  label: {
    fontSize: 16,
    fontWeight: 600,
    width: 100,
    marginBottom: 15,
    color: theme.palette.primary,
  },
  button: {
    width: "auto",
    padding: 10,
    marginBottom: 20,
    color: "white",
  },
  widget: {
    width: "50em",
    padding: "10px 10px",
    backgroundColor: "#FFFFFF",
    height: "auto",
    margin: theme.spacing(4),
  },
  switch: {
    // display: 'block'
  },
  widgetInner: {
    fontSize: "16px",
    display: "inline-flex",
    justifyContent: "space-between",
    width: "100%",
    padding: "16px 16px 0px 16px",
  },
  widgetInnerLabel: {
    paddingTop: "5px",
    fontSize: "16px",
  },
  widgetInnerWrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "0px",
  },
  widgetInnerInput: {
    display: "block",
  },
  mainTwo: {
    display: "flex",
    justifyContent: "flex-start",
    flexDirection: "column",
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(2),
    maxWidth: "70em",
    backgroundColor: "#F5F5F5",
  },
  title: {
    width: "100%",
    padding: "16px",
    display: "inline-flex",
    alignItems: "center",
  },
  regIcon: {
    marginRight: "5px",
  },
  divider: {
    width: "100%",
    height: "2px",
    backgroundColor: "#8ABB2A",
  },
  widgetInputWrapper: {
    display: "inline-flex",
    padding: "16px 16px 19px 16px",
  },
  widgetInputLabel: {
    paddingTop: "5px",
    fontSize: "16px",
    width: "80%",
    textAlign: "left",
  },
  widgetInput: {
    width: "20%",
    padding: "0px 2px",
    border: "1px solid #707070;",
    borderRadius: "4px",
  },
  widgetSlider: {
    padding: "0px 2px",
    marginTop: "30px",
  },
  widgetSliderWrapper: {
    display: "block",
    width: "100%",
    padding: "16px 16px 19px 16px",
  },
  widgetInputLabel: {
    paddingTop: "5px",
    fontSize: "16px",
    width: "80%",
    textAlign: "left",
    marginBottom: "10px",
  },
  widgetInnerSlider: {
    width: '100%',
    display: 'inline-flex'
  },
  widgetMeasureSliderWrapper: {
    display: "block",
    width: "100%",
    padding: "16px 5px 35px 5px",
  },
  sliderButton: {
    transform: 'scale(0.45)',
    backgroundColor: '#707070',
    position: 'relative',
    top: '5px',
    '&:hover': {
      backgroundColor: '#494949' /* Green */,
      opacity: 0.7

    }
  },
  sliderButtonIcon: {
    fill: '#ffffff',
  }
}));


const HumidityWidget = (props) => {

  const capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  const inputHandler = (type, manualModeStatus) => {
    if (type == "measuring") {
      return !manualModeStatus;
    } else {
      return false;
    }
  };


  const handleDecrement = () => {
    let newValue = value - 1
    setValue(newValue)
    props.handleInput(null, newValue)
  }


  const handleIncrement = () => {
    let newValue = value + 1
    setValue(newValue)
    props.handleInput(null, newValue)
  }


  const [value, setValue] = React.useState(props.state);

  const handleValueChange = (event, newValue) => {
    setValue(newValue);
  };

  // const setSliderValue = (humidity) => {
  //   setValue(humidity)
  // }


  
  useEffect(() => {
    setValue(props.state)
  },[props.state])
  

  const handleStateChange = (event, value) => {
    // console.log('COMMITED', value)
  }
  const classes = useStyles();

  return (
    <div>
   

      <Paper
        xs={3}
        className={classes.widget}
        elevation={3}
      >
        <div className={classes.widgetInnerWrapper}>
        <div className={classes.title}>
        <HumidityIcon className={classes.regIcon} />{" "}
        <Typography variant="h6">
          {props.state.toFixed(1)} Rh% {capitalize(props.stateName)}
        </Typography>
      </div>
          <Divider
            className={classes.divider}
            orientation="vertical"
            variant="fullWidth"
          />

            <div className={classes.widgetInner}>
              <label
                className={classes.widgetInnerLabel}
                name={props.stateName}
              >
                Manual Mode
              </label>
              <Switch
                className={classes.switch}
                checked={props.manualRegOn}
                onChange={props.handleSwitch}
                name={props.stateName}
              ></Switch>
            </div>


          <div className={classes.widgetMeasureSliderWrapper}>
      
              <div className = {classes.widgetInnerSlider}>
              <IconButton  onClick={handleDecrement} className = {classes.sliderButton} color="primary" aria-label="upload picture" component="span">
              <RemoveIcon className = {classes.sliderButtonIcon}/>
              </IconButton>
              <Slider
              className={classes.widgetSlider}
                onChange = {handleValueChange}
                onChangeCommitted={props.handleInput}
                disabled={inputHandler(props.type, props.manualRegOn)}
                value={value}
                valueLabelDisplay="auto"
                step={1}
                min={0}
                max={100}
                marks={Marks.humidity}
              ></Slider>
              <IconButton  onClick={handleIncrement} className = {classes.sliderButton} aria-label="upload picture" component="span">
              <AddIcon className = {classes.sliderButtonIcon} />
            </IconButton>
              </div>
      
          </div>

        
        </div>
      </Paper>
    </div>
  );
};

export default HumidityWidget;
