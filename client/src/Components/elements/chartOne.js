import React from "react";
import { RadialBarChart, RadialBar, Legend } from 'recharts';
import { scaleLinear } from 'd3-scale'
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core/";



const ChartOne = (props) => {

  const interpolate = scaleLinear([0, props.openTime], [0, 180])
  
  const useStyles = makeStyles((theme) => ({
    chart: {

    },

  }));

  const data = [
    {
      "name": "TimeOpenVentOne",
      "fill": "#8ABB2A",
      "value": `${(((props.ventOneTimeOpen)/props.openTime)*100).toFixed(1)}`,    
    }
  ]

  const classes = useStyles();
  

  return (
    <div>
    
    <RadialBarChart
    
    barSize={100}
    width={300}
    height={200}
    innerRadius="70%"
    outerRadius="100%"
    data={data}
    // margin = {0} 
    // barSize = {100}
    startAngle={180}
    endAngle={180 -(interpolate(props.ventOneTimeOpen))}
  >

  <RadialBar

      background={true}
      maxAngle={180}
      label={{ fill: '#666', position: 'center' }}
      background clockWise={true}
      dataKey='value'
      />

  </RadialBarChart>
  
    </div>
  );
};

export default ChartOne;

// endAngle={interpolate(props.currentTime)}
// (`${(2500/props.openTime)*100}%`)
// (`${((2500/props.openTime)*100).toFixed(1)}%`)
// `${(((props.ventOne + 2500)/props.openTime)*100).toFixed(1)}%`

// {props.stateName == 'vent 1' && (
      
//   <RadialBarChart

//   width={300}
//   height={200}
//   innerRadius="55%"
//   outerRadius="90%"
//   data={dataVentOne}
//   // margin = {0} 
//   // barSize = {100}
//   startAngle={180}
//   endAngle={180 -(interpolate(props.ventOne + (5000)))}
// >

// <RadialBar
//     minAngle={100}
//     label={{ fill: '#666', position: 'center' }}
//     background = {true} clockWise={true}
//     dataKey='value1'
//     />

// </RadialBarChart>
// )
// }
// {props.stateName == 'vent 2' && (
  
//   <RadialBarChart

//   width={300}
//   height={200}
//   innerRadius="55%"
//   outerRadius="90%"
//   data={dataVentTwo}
//   // margin = {0} 
//   // barSize = {100}
//   startAngle={180}
//   endAngle={180 -(interpolate(props.ventTwo + (10000)))}
// >

// <RadialBar
//     minAngle={100}
//     label={{ fill: '#666', position: 'center' }}
//     background = {true} clockWise={true}
//     dataKey='value2'
//     />

// </RadialBarChart>
// )
// }