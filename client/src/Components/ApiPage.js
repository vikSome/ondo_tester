import React, { useState, useEffect } from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
// import Link from '@material-ui/core/Link';
import { Paper } from '@material-ui/core/';


const ApiPage = (props) => {

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex', 
      flexDirection: 'column',
      minHeight: '90.5vh',
    },
    main: {
      // display: 'flex',
      // justifyContent: 'space-around',
      // marginTop: theme.spacing(8),
      // marginBottom: theme.spacing(2),
      display: 'block'
    },
    contentOne: {
      display: 'flex',
      justifyContent: 'space-between',
      marginTop: theme.spacing(10),
      marginBottom: theme.spacing(2),
      width: '60%'
    },
    contentTwo: {
      display: 'flex',
      justifyContent: 'flex-start',
      marginTop: theme.spacing(8),
      marginBottom: theme.spacing(5),
      
    },
      select: {
        width: 150,
    },
    label: {
      fontSize: 16,
      fontWeight: 600,
      width: 100,
      marginBottom: 15
    },
    button: {
      padding: theme.spacing(1.5),
      display: 'block',
      width: 'auto',
      marginTop: 10,
      fontSize: 18,
      backgroundColor: '#8dc63f',
      color: 'white'
    },
    input: {
      width: 500,
      height: 50,
      fontSize: 30,
      marginTop: 20,
      padding: theme.spacing(3)
    },
    inputFields: {
      display: 'block'
    },
    widgets: {
      padding: theme.spacing(2),
      width:  300,
      backgroundColor:
          theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[800],
    }
   
  }));

  const classes = useStyles();

    const callBackendAPI = async () => {
        const response = await fetch('/home');
        const body = await response.json();
    
        if (response.status !== 200) {
          throw Error(body.message) 
        }
        return body;
      };
    
      const callBackendApiNew = async () => {
        const response = await fetch('/api');
        const body = await response.json();
        
        if (response.status !== 200) {
          throw Error(body.message) 
        }
        return body;
      };



    const [apiNew, setApiNew] = useState("");
    useEffect(() => {
      callBackendAPI() 
      .then(res => setApiNew(res.home.temperature))
      .catch(err => console.log(err));
     }, 
    []
  );

  const [api, setApi] = useState("");
  useEffect(() => {
    callBackendAPI() 
    .then(res => setApi(res.home.humidity ))
    .catch(err => console.log(err));
   }, 
  []
);


    return (
      <div className={classes.root}>
      <CssBaseline />
      <Container component="main" className={classes.main} maxWidth="lg">
        <div className = {classes.contentTwo}>
        <div className = {classes.contentOne}>
        <Paper className={classes.widgets}>
        <Typography variant='h5' component='h4'>API humidity: <span className = "api-page__input">{api}</span></Typography>
        </Paper>
        <Paper className={classes.widgets}>
        <Typography variant='h5' component='h4'>API temperature: <span className = "api-page__input">{apiNew}</span></Typography>
        </Paper>
        </div>  
        </div>
        </Container>
        </div>
    )

}

export default ApiPage;


