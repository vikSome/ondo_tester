export default {
    DI_BIT: {
        bitSetDI0: 'MainPump',
        bitSetDI1: 'MixerChValve 1',
        bitSetDI2: 'MixerChValve 1',
        bitSetDI3: 'Valve 1',
        bitSetDI4: 'Valve 2',
        bitSetDI5: 'Valve 3',
        bitSetDI6: 'Valve 4',
        bitSetDI7: 'VentLine 1 Open',
        bitSetDI8: 'VentLine 1 Close',
        bitSetDI9: 'VentLine 2 Open',
        bitSetDI10: 'VentLine 2 Close',
        bitSetDI11: 'Circulation Fan 1',
        bitSetDI12: 'Circulation Fan 2',
        bitSetDI13: 'MistingLine 1',
        bitSetDI14: 'MistingLine 2',
        bitSetDI15: 'Empty'
    },
    DO_BIT: {
        bitSetDO0: 'Main Watermeter Pulse',
        bitSetDO1: 'Fertimeter 1 Pulse',
        bitSetDO2: 'Fertimeter 2 Pulse',
        bitSetDO3: 'Vent line 1 open limit switch status',
        bitSetDO4: 'Vent line 1 close limit switch status',
        bitSetDO5: 'Vent line 2 open limit switch status',
        bitSetDO6: 'Vent line 2 close limit switch status',
        bitSetDO7: 'Empty',
        bitSetDO8: 'Empty',
        bitSetDO9: 'Empty',
        bitSetDO10: 'Empty',
        bitSetDO11: 'Empty',
        bitSetDO12: 'Empty',
        bitSetDO13: 'Empty',
        bitSetDO14: 'Empty',
        bitSetDO15: 'Empty'
    }
}