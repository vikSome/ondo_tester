import definitions from './definitions'

const maskList = [
  {
    bit: 0,
    onMask_bit: 1,
    name: "setting_1",
    DI: {
      config: definitions.DI_BIT.bitSetDI0
    }
  },
  {
    bit: 1,
    onMask_bit: 2,
    name: "setting_2"
  },
  {
    bit: 2,
    onMask_bit: 4,
    name: "setting_3"
  },
  {
    bit: 3,
    onMask_bit: 8,
    name: "setting_4"
  },
  {
    bit: 4,
    onMask_bit: 16,
    name: "setting_5"
  },  
  {
    bit: 5,
    onMask_bit: 32,
    name: "setting_6"
  },
  {
    bit: 6,
    onMask_bit: 64,
    name: "setting_7"
  },
  {
    bit: 7,
    onMask_bit: 128,
    name: "setting_8"
  },
  {
    bit: 8,
    onMask_bit: 256,
    name: "setting_9"
  },
  {
    bit: 9,
    onMask_bit: 512,
    name: "setting_10"
  },
  {
    bit: 10,
    onMask_bit: 1024,
    name: "setting_11"
  },  
  {
    bit: 11,
    onMask_bit: 2048,
    name: "setting_12"
  },
  {
    bit: 12,
    onMask_bit: 4096,
    name: "setting_13"
  },
  {
    bit: 13,
    onMask_bit: 8192,
    name: "setting_14"
  },
  {
    bit: 14,
    onMask_bit: 16384,
    name: "setting_15"
  },
  {
    bit: 15,
    onMask_bit: 32768,
    name: "setting_16"
  }
]

export default maskList;