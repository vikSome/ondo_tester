export default  {
    temperature: 
    [           
        {
                value: 0,
                label: '0°C'
                },
                {
                value: 10,
                label: '10°C'
                },
                {value: 20,
                label: '20°C'
                },
                {value: 30,
                label: '30°C'
                },
                {value: 40,
                label: '40°C'
                },{value: 50,
                label: '50°C'
                }
    ],


humidity: [
                {
                  value: 0,
                  label: '0%'
                },
                {
                 value: 10,
                 label: '10%'
                },
                {
                 value: 20,
                 label: '20%'
                },
                {
                 value: 30,
                 label: '30%'
                },
                {
                 value: 40,
                 label: '40%'
                },
                {
                 value: 50,
                 label: '50%'
                },
                {
                 value: 60,
                 label: '60%'
                },
                {
                 value: 70,
                 label: '70%'
                },
                { 
                 value: 80,
                 label: '80%'
                 },
                 {
                  value: 90,
                  label: '90%'
                 },
                 {
                  value: 100,
                  label: '100%'
                 }
],
ventOpenTIme: 
[
    {
        value: 0,
        label: '0s'
      },
      {
       value: 100,
       label: '100s'
      },
      {
       value: 200,
       label: '200s'
      },
      {
       value: 300,
       label: '300s'
      },  
      {
       value: 400,
       label: '400s'
      },
      {
       value: 500,
       label: '500s'
      },
       {
        value: 600,
        label: '600s'
       },
],
pulses: 
[
  {
    value: 0,
    label: '0s'
  },
  {
   value: 1,
   label: '1s'
  },
  {
   value: 2,
   label: '2s'
  },
  {
   value: 3,
   label: '3s'
  },
  {
   value: 4,
   label: '4s'
  },
  {
   value: 5,
   label: '5s'
  },
  {
   value: 6,
   label: '6s'
  },
  {
   value: 7,
   label: '7s'
  },
  { 
   value: 8,
   label: '8s'
   },
   {
    value: 9,
    label: '9s'
   },
   {
    value: 10,
    label: '10s'
   }
]
}

// {
//     value: 0,
//     label: '0s'
//   },
//   {
//    value: 50,
//    label: '50s'
//   },
//   {
//    value: 100,
//    label: '100s'
//   },
//   {
//    value: 150,
//    label: '150s'
//   },
//   {
//    value: 200,
//    label: '200s'
//   },
//   {
//     value: 250,
//     label: '250s'
//    },
//   {
//    value: 300,
//    label: '300s'
//   },  
//   {
//     value: 350,
//     label: '350s'
//    },
//   {
//    value: 400,
//    label: '400s'
//   },
//   {
//     value: 450,
//     label: '450s'
//    },
//   {
//    value: 500,
//    label: '500s'
//   },
//   { 
//    value: 550,
//    label: '550s'
//    },
//    {
//     value: 600,
//     label: '600s'
//    },