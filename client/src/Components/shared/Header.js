import React, { useEffect, useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {Tab, FormLabel, Button, Form, InputLabel} from '@material-ui/core';
import { ReactComponent as OndoLogo } from '../../assets/ondoLogo.svg';
import { ReactComponent as TestLogo } from '../../assets/testAppLogo.svg';
import CircularProgress from '@material-ui/core/CircularProgress';



const useStyles = makeStyles((theme) => ({
    icon: {
      marginRight: theme.spacing(2),
      color: 'white'
    },
    toolbarLink: {
        padding: theme.spacing(2),
        flexShrink: 0,
        fontSize: 20,
        fontWeight: 700,
        color: 'white',
      },
      navText: {
          marginRight: theme.spacing(1),
          color: 'white'
      },
      toolBarWrapper: {
        width: '70%',
        display: 'inline-flex',
        justifyContent: 'center'
      },
      logo: {
        display: 'inline-flex',
        justifyContent: 'space-between',
        paddingLeft: '56px',
        
      },
      AppBar: {
        display: "flex",
      },
      testLogo: {
        marginLeft: '15px'
      },
      plcIP: {
        display: 'inline-flex',
        height: '20px'
      },
      plcIpText: {
        width: '280px',
        textAlign: 'right',
        paddingRight: '5px'

      },
      plcIpForm: {
        display: 'inline-flex',
      },
      plcIpFormWrapper: {
        display: 'inline-flex',
        height: '20px'
      },
      formInputLabel: {
        textAlign: 'right',
        width: '60px',
        paddingRight: '5px'
      },
      formInputIp: {
        width: '110px',
        marginRight: '5px'
      },
      formInputPort: {
        width: '60px',
        marginRight: '5px'
      },
      loadingSpinner: {
        transform: 'scale(0.45)',
      } 
      }));


const Header = (props) => {

  const callBackendAPI = async (path) => {
    // debugger
    const response = await fetch(path);
    const body = await response.json();
    if (response.status !== 200) {
      throw Error(body.message)
    }
    return body;
  };

  const callBackendPlcAPi = async (path) => {
    // debugger
    const response = await fetch(path);
    const body = await response.json();
    if (response.status !== 200) {
      throw Error(body.message)
    }
    return body;
  };

  const plcConnectionApi = async (IP = '', Port = '',Id = 1, path = 'closePlcConnection') => {
  // debugger
  const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({plcIp: IP, plcPort: Port, plcId: Id})
  };
  await setLoading(true);
  const response = await fetch(`http://127.0.0.1:5000/api/${path}`, requestOptions);
  await response.json();
  await setLoading(false);

  }

  const[plcIp, setPlcIp] = useState();
  const[plcPort, setPlcPort] = useState();
  const[plcStatus, setPlcStatus] = useState(false);
  const[plcValueIp, setPlcValueIp] = useState();
  const[plcValuePort, setPlcValuePort] = useState('502');
  const[loading, setLoading] = useState(false);


  useEffect(() => {
    callBackendAPI('/home')
    .then(res => setPlcIp(res.home.plcIp))
    .catch(err => console.log(err));

    callBackendAPI()
    .then(res => setPlcPort(res.home.plcPort))
    .catch(err => console.log(err));

    callBackendAPI('/home')
    .then(res => setPlcStatus(res.home.plcStatus))
    .catch(err => console.log(err));

    props.socket.on("PLC_CONNECTION_CHANGE_OPEN", async data => {

      // console.log('PLC_DATA', data)

      setPlcIp(data.plcIp);
      setPlcPort(data.plcPort);
      setPlcStatus(true);
      
      // if(data.plcStatus) {
      //   setPlcStatus(true)
      // } else if (!data.plcStatus) {
      //   setPlcStatus(false)
      // };
      
    });

    props.socket.on("PLC_CONNECTION_CHANGE_CLOSE", async data => {
      // console.log('PLC_DATA', data)
      
      setPlcIp(data.plcIp);
      setPlcPort(data.plcPort);
      setPlcStatus(false);
      
      // if(data.plcStatus) {
      //   setPlcStatus(true)
      // } else if (!data.plcStatus) {
      //   setPlcStatus(false)
      // };
      
    });


    props.socket.on("PLC_CONNECTION_RESET", async data => {

      // console.log('PLC_DATA', data)

      setPlcIp(data.plcIp);
      setPlcPort(data.plcPort);
      
      if(data.plcStatus) {
        setPlcStatus(true)
      } else if (!data.plcStatus) {
        setPlcStatus(false)
      }
    });


  });


  // const setNewPlcConnection = (newValue, name) => {
  
  //   // setVentOneOpenTime(newValue);
  //   setManualReg(newValue, 'NEW_PLC_CONNECTION');

  // }

  const setNewPlcConnection = (value, name) => {
      props.socket.emit(name, value);

  };

  const closePlcConnection = () => {
    plcConnectionApi();
    
    // props.socket.emit('CLOSE_PLC_CONNECTION', {'plcIp': '', plcPort: '', 'plcId': 1})
  };

  const handleSubmit = (e) => {
    
    e.preventDefault();
    // setPlcIp(e.target.value);
    // setNewPlcConnection({'plcIp': plcValueIp, 'plcPort': plcValuePort, 'plcId': 1}, 'NEW_PLC_CONNECTION');
    plcConnectionApi(plcValueIp, plcValuePort, 1, 'openPlcConnection');

  };

  const handlePlcChangeIp = (e) => {
    // console.log('HANDLE_PLC_CHANGE_INPUT', e)
    setPlcValueIp(e.target.value)
  };

  const handlePlcChangePort = (e) => {
    // console.log('HANDLE_PLC_CHANGE_INPUT', e)
    setPlcValuePort(e.target.value)
  };

  const handlePlcConnectionKill = () => {
      closePlcConnection();

      // callBackendPlcAPi('/api/closePlcConnection');
  };

  const handlePlcConnectionOn = () => {
    // debugger
    // callBackendPlcAPi('/api/openPlcConnection');
  };

  const loadingSpinner = (() => {
    return (
      <div>
    <CircularProgress size = {20} thickness ={3.6} className={classes.loadingSpinner}/>
    <CircularProgress size = {20} thickness ={3.6} color="secondary" />
      </div>
    )
  })

    const classes = useStyles();
      return (
        <div>
        <CssBaseline />
        <AppBar position="relative" className = {classes.AppBar}>
          <Toolbar >
            <div className = {classes.logo}>
            <OndoLogo />
            <TestLogo className = {classes.testLogo} />
            {console.log('LOADING', loading)}
            </div>
            <div className = {classes.toolBarWrapper}>
                <Tab className={classes.toolbarLink} label = 'Home' href ={'/'}>                
                </Tab>

                <Tab className={classes.toolbarLink} label = 'Api' href ={'/api'}>            
                </Tab>
                </div>

                {!plcStatus && 
                  <div className={classes.plcIpForm}>
                  {loading && loadingSpinner()}
                  <form onSubmit={handleSubmit} name='PlcIp' className={classes.plcIpFormWrapper}>
                  <label name='Plc' color='white' className={classes.formInputLabel}>Plc Ip:</label>
                  <input required={true} type='text' placeholder='0.0.0.0.0' name='PlcIp' className={classes.formInputIp} onChange={handlePlcChangeIp}></input>
                  <label name='PlcPort' color='white' className={classes.formInputLabel}>Plc Port:</label>
                  <input required={true} type='text' placeholder='000' name='PlcPort' defaultValue='502' className={classes.formInputPort} onChange={handlePlcChangePort}></input>
                  <input type='submit' value='Connect' onClick={handlePlcConnectionOn}></input>
                  </form>
                  </div>
                 }

                {plcStatus && 
                  <div className={classes.plcIP}>
                  <Typography className={classes.plcIpText}>Connected to: {plcIp} : {plcPort}</Typography>
                  <button onClick={handlePlcConnectionKill}>disconnect</button>
                  </div>
                 }
                
          </Toolbar>
        </AppBar>
        </div>
      )

}

export default Header;