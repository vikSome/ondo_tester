import React, { useState, useEffect } from "react";
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Paper, Switch, Input, TextField, Tooltip } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import BitIcon from '@material-ui/icons/FiberManualRecord';
import maskList from './utils/maskList';
import checker from './utils/maskChecker';
import bitDef from './utils/definitions';
import VentTwoWidget from './elements/ventTwoWidget';
import HumidityWidget from './elements/humidityWidget';
import TemperatureWidget from './elements/temperatureWidget';
import VentOneWidget from './elements/ventOneWidget';
import PulseWidget from './elements/pulseWidget';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '90.5vh',
    backgroundColor: '#F5F5F5'
  },
  main: {
    display: 'flex',
    justifyContent: 'flex-start',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
    // maxWidth: '550vw',
    maxWidth: '200em',
    backgroundColor: '#F5F5F5'
  },
  select: {
    width: 150,
  },
  label: {
    fontSize: 16,
    fontWeight: 600,
    width: 100,
    marginBottom: 15
  },
  button: {
    width: 'auto',
    padding: 10,
    marginBottom: 20,
    color: 'white'
  },
  widget: {
    width: '50em',
    padding: "20px 10px",
    backgroundColor: '#eceff1',
    height: '35vh',
    margin: theme.spacing(4),
    border: '2px solid grey',
  },
  switch: {
    // display: 'block'

  },
  widgetInner: {
    margin: "15px 0px",
    display: "inline-flex",
    textAlign: "left",
    width: "70%",
    padding: "0px",

  },
  widgetInnerLabel: {
    paddingTop: "5px",
    fontSize: "20px",
  },
  widgetInnerWrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  widgetInnerInput: {
    display: 'block'
  },
  mainTwo: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginTop: theme.spacing(6),
    marginRight: theme.spacing(4),
    marginBottom: theme.spacing(2),
    maxWidth: '70em',
    backgroundColor: "#F5F5F5",
    padding: '10px 10px',
  
  },
  widgetBit: {
    display: 'inline-flex',
    width: "50em",
    padding: "20px 10px 10px 10px",
    backgroundColor: '#FFFFFF',
    margin: theme.spacing(4),
    marginTop: theme.spacing(2),
    borderTop: `2px solid ${theme.palette.secondary.main}`,
    marginLeft: theme.spacing(6),
  },
  BitIcon: {
    fontSize: '30px'
  },
  bitLabel: {
    fontWeight: '500',
    fontSize: '12px'
  },
  widgetBitTitle: {
    display: 'inline-flex',
    marginLeft: theme.spacing(6),
  },
  mainElementOne: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(4),
    marginBottom: theme.spacing(2),
    backgroundColor: '#F5F5F5',
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    maxWidth: '57em'
  },
  mainElementTwo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(2),
    backgroundColor: '#F5F5F5',
    marginLeft: theme.spacing(0),
    marginRight: theme.spacing(0),
    maxWidth: '70em'

  },
  bitSetInner: {
    textAlign: 'center'
  }
}));


const PlcComponent = (props) => {

  
  const [switchesEnabled, setSwitchesEnabled] = React.useState({
    humidity: true,
    temperature: true,
    checkedF: true,
    checkedG: true,
  });
  
  const [bitColor, setBitColor] = React.useState({
    on: '#8ABB2A',
    off: '#EA3838',
  });

  const [humidity, setHumidity] = useState(0);
  const [temperature, setTemperature] = useState(0);
  const [ventOneTimeOpen, setVentOneTimeOpen] = useState(0);
  const [ventTwoTimeOpen, setVentTwoTimeOpen] = useState(0);
  const [mainWaterMeterPulse, setMainWaterMeterPulse] = useState(0);
  const [mixerChannelOnePulse, setMixerChannelOnePulse] = useState(0);
  const [mixerChannelTwoPulse, seMixerChannelTwoPulse] = useState(0);
  const [bitEncodedOne, setBitEncodedOne] = React.useState({
    name: "bitSetDI",
    value: 0,
  });

  const [bitEncodedTwo, setBitEncodedTwo] = React.useState({
    name: "bitSetDO",
    value: 0,
  });

  const callBackendAPI = async () => {
    const response = await fetch('/home');
    const body = await response.json();
    if (response.status !== 200) {
      throw Error(body.message)
    }
    return body;
  };


  const [humiditySlider, setHumiditySlider] = useState();
  const [temperatureSlider, setTemperatureSlider] = useState();
  const [ventOneOpenTimeSlider, setVentOneOpenTimeSlider] = useState(0);
  const [ventTwoOpenTimeSlider, setVentTwoOpenTimeSlider] = useState(0);

  const setDefaultSliderValue = (value) => {
    let sliderDefault = value;
    return sliderDefault

  }
  useEffect(() => {


//Call API on rerendering 

    callBackendAPI()
      .then(res => setHumidity(res.home.humidity))
      .catch(err => console.log(err));

      callBackendAPI()
      .then(res => setHumiditySlider(res.home.humidity))
      .catch(err => console.log(err));

    callBackendAPI()
      .then(res => setTemperature(res.home.temperature))
      .catch(err => console.log(err));

      callBackendAPI()
      .then(res => setTemperatureSlider(res.home.temperature))
      .catch(err => console.log(err));

    callBackendAPI()
      .then(res => setVentOneTimeOpen(res.home.ventOneTimeOpen))
      .catch(err => console.log(err));
    
    callBackendAPI()
    .then(res => setVentOneOpenTime(res.home.ventOneOpenTime))
    .catch(err => console.log(err));

    callBackendAPI()
    .then(res => setVentOneOpenTimeSlider(res.home.ventOneOpenTime))
    .catch(err => console.log(err));

    callBackendAPI()
      .then(res => setVentTwoTimeOpen(res.home.ventTwoTimeOpen))
      .catch(err => console.log(err));

    callBackendAPI()
    .then(res => setVentTwoOpenTime(res.home.ventTwoOpenTime))
    .catch(err => console.log(err));

    callBackendAPI()
    .then(res => setVentTwoOpenTimeSlider(res.home.ventTwoOpenTime))
    .catch(err => console.log(err));

    callBackendAPI()
      .then(res => setBitEncodedTwo({ value: res.home.BitEncodedTwo, name: "bitSetDO" }))
      .catch(err => console.log(err));

    callBackendAPI()
      .then(res => setBitEncodedOne({ value: res.home.BitEncodedOne, name: "bitSetDI" }))
      .catch(err => console.log(err));


// Update states when backend changes

    props.socket.on("HUMIDITY_CHANGE", async data => {
      console.log('HUMIDITY_CHANGE_NOW')
      setHumidity(data)
    });

    props.socket.on("TEMPERATURE_CHANGE", async data => {
      // console.log("TEMPERATURE_CHANGE", data)

      setTemperature(data);
    });

    props.socket.on("VENT_ONE_TIME_OPEN_CHANGE", async data => {

      setVentOneTimeOpen(data);
    });

    props.socket.on("VENT_TWO_TIME_OPEN_CHANGE", async data => {

      setVentTwoTimeOpen(data);
    });

    props.socket.on("VENT_ONE_OPEN_TIME_CHANGE", async data => {

      setVentOneOpenTime(data);
    });

    props.socket.on("VENT_TWO_OPEN_TIME_CHANGE", async data => {

      setVentTwoOpenTime(data);
    });


    props.socket.on("BIT_ENCODED_ONE_CHANGE", async data => {
      setBitEncodedOne({ value: data, name: "bitSetDI" });
    });

    props.socket.on("BIT_ENCODED_TWO_CHANGE", async data => {
      setBitEncodedTwo({ value: data, name: "bitSetDO" });
    });

    props.socket.on("PLC_CONNECTION_CHANGE_CLOSE", async data => {
      // console.log('PLC_CONNECTION_RESET', data)
      if(data.plcStatus === 0 ){
        setHumidity(0);
        setTemperature(0);
        setBitEncodedOne(0);
        setBitEncodedTwo(0);
        setVentOneOpenTime(0);
        setVentTwoOpenTime(0);
        setVentOneTimeOpen(0);
        setVentTwoTimeOpen(0);
      }
    
    });

    // props.resetGlobalState;
    // props.socket.on("PLC_CONNECTION_RESET", async data => {

    //   // console.log('PLC_DATA', data)

    //   setHumidity(data.humidity);
    //   setTemperature(data.temperature);
    //   setVentOneOpenTime(data.ventOneOpenTime);
    //   setVentTwoOpenTime(data.ventTwoOpenTime);
    //   setVentOneTimeOpen(data.ventTwoTimeOpen);
    //   setVentTwoTimeOpen(data.ventTwoTimeOpen);
    //   setBitEncodedOne(data.bitEncodedOne);
    //   setBitEncodedTwo(data.bitEncodedTwo);
      
    // });


    // setBitEncodedOne({ value: 2**7, name: "bitSetDI" })
    // setBitEncodedTwo({ value: 2**3, name: "bitSetDO" })
    // iterator(bitEncodedTwo);
    // iterator(bitEncodedOne);

  },
    []
  );

// Temperature and Humidity Manual/Dial mode switch 

  const setStatus = (value) => {

    if (value || value == 0) {
      props.socket.emit('MEASURING_BOX_CONFIG', value);

    }
  }

  const switchSettings = (event) => {

    let mask = 0;
    const newState = { ...switchesEnabled, [event.target.name]: event.target.checked };

    if (newState.humidity) {
      mask |= 1;
    }

    if (newState.temperature) {
      mask |= 1 << 1;
    }

    setStatus(mask);

    setSwitchesEnabled(newState);

  }


  // const temperatureHandler = () => {
  //   let newButtonStatus = { ...regButtonStatus, ['temperature']: !regButtonStatus.temperature };
  //   setRegButtonStatus(newButtonStatus);

  // }

  // const humidityHandler = () => {
  //   let newButtonStatus = { ...regButtonStatus, ['humidity']: !regButtonStatus.humidity };
  //   setRegButtonStatus(newButtonStatus);

  // }

  // const ventOneHandler = () => {
  //   let newButtonStatus = { ...regButtonStatus, ['ventOne']: !regButtonStatus.ventOne };
  //   setRegButtonStatus(newButtonStatus);

  // }

  // const ventTwoHandler = () => {

  //   let newButtonStatus = { ...regButtonStatus, ['ventTwo']: !regButtonStatus.ventTwo };
  //   setRegButtonStatus(newButtonStatus);

  // }

  const regButtonStatusHandler = () => {
    let newButtonStatus;
  }


  // Send events to the backend with new PLC registers' values

  const [humidityManual, setHumidityManual] = useState();
  const [temperatureManual, setTemperatureManual] = useState();
  const [ventOneOpenTime, setVentOneOpenTime] = useState(0);
  const [ventTwoOpenTime, setVentTwoOpenTime] = useState(0);


  const handleManualModeHumidity = (e, newValue) => {
    // setManualReg(e.target.value, 'newHumidityReg');
    // console.log('ONCHANGECOMMITED', newValue)
  
    setManualReg(newValue, 'NEW_HUMIDITY_REG');
    setHumidityManual(newValue);
  }

  // const handleManualModeHumidityButton = (newValue) => {
  //   // setManualReg(e.target.value, 'newHumidityReg');
  //   console.log(newValue)
  
  //   setManualReg(newValue, 'NEW_HUMIDITY_REG');
  //   setHumidityManual(newValue);
  // }

  const handleManualModeTemperature = (e, newValue) => {
   
    setManualReg(newValue, 'NEW_TEMPERATURE_REG');
    setTemperatureManual(newValue);

  }

  // const handleManualModeTemperatureButton = (newValue) => {
  //   setManualReg(newValue, 'NEW_TEMPERATURE_REG');
  //   setTemperatureManual(newValue);

  // }

  const handleManualModeVentOne = (e, newValue) => {
  
    setVentOneOpenTime(newValue);
    setManualReg(newValue, 'NEW_VENT_ONE_OPEN_TIME');

  }

  const handleManualModeVentTwo = (e, newValue) => {
    setManualReg(newValue, 'NEW_VENT_TWO_OPEN_TIME');
    setVentTwoOpenTime(newValue);

  }

  const handleMainWaterMeterPulse = (e, newValue) => {
    // setManualReg(e.target.value, 'newHumidityReg');
    let pulseBase = ((newValue*1000)/100)
    // console.log('ONCHANGECOMMITED_MAIN_PULSE', pulseBase)
    // console.log('ONCHANGECOMMITED_MAIN_PULSE', newValue)
    setManualReg(pulseBase, 'NEW_MAIN_WATERMETER_PULSE_REG');
    setMainWaterMeterPulse(newValue);
  }

  const handleMixerChannelOnePulse = (e, newValue) => {
    // setManualReg(e.target.value, 'newHumidityReg');
    let pulseBase = ((newValue*1000)/100)
    // console.log('ONCHANGECOMMITED', newValue)
  
    setManualReg(pulseBase, 'NEW_MIXER_CHANNEL_ONE_PULSE_REG');
    setMixerChannelOnePulse(newValue);
  }

  const handleMixerChannelTwoPulse = (e, newValue) => {
    // setManualReg(e.target.value, 'newHumidityReg');
    let pulseBase = ((newValue*1000)/100)
    // console.log('ONCHANGECOMMITED', newValue)
  
    setManualReg(pulseBase, 'NEW_MIXER_CHANNEL_TWO_PULSE_REG');
    seMixerChannelTwoPulse(newValue);
  }

  const setManualReg = (value, name) => {
    //  let name;
    if (humidityManual) {
      // name = 'newHumidityReg'
      // props.setManualReg(value, name)
      props.socket.emit(name, value)
    } else if (temperatureManual) {

      // name = 'newTemperatureReg'
      // props.setManualReg(value, name)
      props.socket.emit(name, value)
    } else if (ventOneOpenTime) {
  
      props.socket.emit(name, value)
    } else if (ventTwoOpenTime) {
      // name = 'newTemperatureReg'
      // props.setManualReg(value, name)
      props.socket.emit(name, value)
    }

  };

  const BitSet = (bits, name, state, label) => {

    let i;
    let x = [];
    let z = 1;

    for (i = bits; i >= 0; i--) {

      let check = checker(
        maskList[maskList.length - z]["onMask_bit"],
        state.value, maskList[maskList.length - z]["bit"])

      x.push(
        <Tooltip key={`tooltip_${name}${i}`} title={name == 'bitSetDI' ? `${bitDef.DI_BIT[name + i]}` : `${bitDef.DO_BIT[name + i]}`} placement="right">
        <div key={`section_${name}${i}`} className = {classes.bitSetInner}>
      
          <label
            className={classes.bitLabel}
            name={`${name}${i}`}
          >{`${label} ${i}`}
          </label>
          
          <BitIcon
            key={`${name}${i}`}
            id={`${name}${i}`}
            className={classes.BitIcon}
            style={{
              fill: check ? bitColor.on : bitColor.off
            }}
          /> 
          
        </div>
        </Tooltip>
      )
      z++;
    }
    return x
  };


//   const [ventOneSliderDefaultValue, setVentOneSliderDefaultValue] = React.useState();
//   const [ventTwoSliderDefaultValue, setVentTwoSliderDefaultValue] = React.useState();


//   useEffect(() => {

//       callBackendAPI()
//       .then((res) => setVentOneSliderDefaultValue(res.home.ventOneOpenTime))
//       .catch((err) => console.log(err));

//       callBackendAPI()
//       .then((res) => setVentTwoSliderDefaultValue(res.home.ventTwoOpenTime))
//       .catch((err) => console.log(err));
// }
//   );

  const classes = useStyles();

  return (
    <div className={classes.root}>
    
      <CssBaseline />

      <Container component="main" className={classes.main} maxWidth="lg">


      <Container component="main" className={classes.mainElementOne} maxWidth="lg">
        
      <HumidityWidget

          setManualReg = {setHumidity}
          manualRegOn={switchesEnabled.humidity}
          handleSwitch={switchSettings}
          handleInput={handleManualModeHumidity}
          state={parseInt(humidity)}
          stateName={'humidity'}
          type={'measuring'}
          sliderDefault = {humiditySlider}

        />

        <TemperatureWidget

          callBackendAPI={temperatureSlider}
          setManualReg = {setTemperature}
          manualRegOn={switchesEnabled.temperature}
          handleSwitch={switchSettings}
          handleInput={handleManualModeTemperature}
          state={parseInt(temperature)}
          stateName={'temperature'}
          type={'measuring'}
          sliderDefault={temperatureSlider}
        />  

  </Container>

      <Container component="main" className={classes.mainElementTwo} maxWidth="lg">
        <VentOneWidget

          callBackendAPI={callBackendAPI}
          sliderDefault={ventOneOpenTimeSlider}
          ventOneTimeOpen={ventOneTimeOpen}
          openTime={ventOneOpenTime}
          opening={bitEncodedOne.value & maskList[7]["onMask_bit"] ? true : false}
          closing={bitEncodedOne.value & maskList[8]["onMask_bit"] ? true : false}
          openSwitch={bitEncodedTwo.value & maskList[3]["onMask_bit"] ? false : true}
          closeSwitch={bitEncodedTwo.value & maskList[4]["onMask_bit"] ? false : true}
          handleInput={handleManualModeVentOne}
          stateName={'vent 1'}
          type={'vent'}

        />

        <VentTwoWidget
          sliderDefault={ventTwoOpenTimeSlider}
          ventTwoTimeOpen={ventTwoTimeOpen}
          openTime={ventTwoOpenTime}
          opening={bitEncodedOne.value & maskList[9]["onMask_bit"] ? true : false}
          closing={bitEncodedOne.value & maskList[10]["onMask_bit"] ? true : false}
          openSwitch={bitEncodedTwo.value & maskList[5]["onMask_bit"] ? false : true}
          closeSwitch={bitEncodedTwo.value & maskList[6]["onMask_bit"] ? false : true}
          handleInput={handleManualModeVentTwo}
          stateName={'vent 2'}
          type={'vent'}
    
        />

      </Container>
      </Container>

      <Container component="main" className={classes.main} maxWidth="lg">

      <div component="main" className={classes.mainTwo} maxWidth="lg">

        <Typography variant='h5' className={classes.widgetBitTitle}> Register 40003 - Input </Typography>
        <Paper xs={3} className={classes.widgetBit}>
          {BitSet(15, 'bitSetDI', bitEncodedOne, 'DI')}
        </Paper>

        <Typography variant='h5' className={classes.widgetBitTitle}> Register 40005 - Output </Typography>
        <Paper xs={3} className={classes.widgetBit}>
          {BitSet(15, 'bitSetDO', bitEncodedTwo, 'DO')}
        </Paper>

      </div>

      <div component="main" className={classes.mainTwo} maxWidth="lg">

      <PulseWidget

      setManualReg = {setMainWaterMeterPulse}
      handleInput={handleMainWaterMeterPulse}
      state={parseInt(mainWaterMeterPulse)}
      stateName={'Main watermeter pulse'}

    />

    <PulseWidget

    setManualReg = {setMixerChannelOnePulse}
    handleInput={handleMixerChannelOnePulse}
    state={parseInt(mixerChannelOnePulse)}
    stateName={'Mixer channel 1 pulse'}

  />

  <PulseWidget

  setManualReg = {seMixerChannelTwoPulse}
  handleInput={handleMixerChannelTwoPulse}
  state={parseInt(mixerChannelTwoPulse)}
  stateName={'Mixer channel 2 pulse'}

/>

      </div>
      </Container>
    </div>
  )
}

export default PlcComponent;



