import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../Components/shared/Header';
import Footer from '../Components/shared/Footer';
import ApiPage from '../Components/ApiPage';
import AppDashboard from '../Components/AppDashboard';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import socketIOClient from "socket.io-client";

const ENDPOINT = "http://127.0.0.1:5000/";

// const theme = createMuiTheme ({
//     palette: {
//         primary: { 
//         main: '#494949'
//     },
//     secondary: { 
//         main: '#356859'
//     }
//     }
// })

const AppRouter = (state) => {

const socket = socketIOClient(ENDPOINT);

// const resetGlobalState = (status) => {
//     let reset = false;
//     if(status === 0) {
//         reset = true
//     }

//     return reset
// }

return (
    
    <BrowserRouter>
    <div>
    <Header socket={socket} />
    <Switch>
            <Route path="/" component={ApiPage} exact={true}/>
            <Route path="/api" component={AppDashboard} exact={true}/>
    </Switch>
    <Footer />
    </div>
    </BrowserRouter> 
   
 
)};

export default AppRouter;