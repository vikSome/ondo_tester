class PlcController {
    constructor(apiRoot){
        this.apiRoot = apiRoot;

    };

    connectionRoutes () {
        return [
            {
                route: `${this.apiRoot}\openPlcConnection`,
            },
            {
                route: `${this.apiRoot}\closePlcConnection`
            } 
        ]

    }
};