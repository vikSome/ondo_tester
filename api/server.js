const eventBus = require('js-event-bus')();

const {
  ApiService,
  SocketsService,
  PLCService,
  StateManager
} = require('./services').services;

const apiRoot = '/api';
// const plcIp = '192.168.16.19'
// const plcPort = 502
// const plcId = 1

class Server {

  constructor() {
    this.plcService = new PLCService();
  }

  initApiService() {
    this.apiService = new ApiService(
      apiRoot,
      this.stateManager,
      this.plcService
    )
  }

  async initStateManager() {
    this.stateManager = new StateManager();
    await this.stateManager.init();
  }

  // async connectToPLC(plcIp, plcPort, plcId) {

  //   try {
  //     await this.plcService.connect(plcIp, plcPort, plcId);
  //   }
  //   catch (err) {
  //     console.log(err, 'Plc Connection Error');
  //   }
  // }

  async initSocketService() {
    this.socketService = new SocketsService(this.apiService).start();
  }
  async start() {
    // try {
    //   await this.initPlcServices();
    // } catch (err) {
    //   console.log(err, 'Error INIT')
    // }

    try {
      await this.initStateManager();
    } catch (err) {
      console.log(err, 'Error State');
    }

    await this.initApiService();

    await this.initSocketService();

    // eventBus.on('NEW_PLC_CONNECTION', (data) => {
    // this.connectToPLC(data.plcIp, data.plcPort, data.plcId)
    // });
    // eventBus.on('CLOSE_PLC_CONNECTION', () => this.plcService.disconnect());
  }
}



(async function start() {
  process.on('uncaughtException', (e) => console.log(e));
  const server = new Server;
  await server.start();
})().catch(e => { console.error(e) });
