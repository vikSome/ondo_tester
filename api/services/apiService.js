const express = require('express');
const cors = require('cors');
const http = require('http');
const bodyParser = require('body-parser');
const EventEmitter = require('events');
const fs = require('fs');
const { json } = require('express');
const eventBus = require('js-event-bus')();;

class ApiService extends EventEmitter {
    /**
     * 
     * @param {String} apiRoot 
     * @param {import('./stateManager')} stateManager 
     */
    constructor(apiRoot, stateManager, plcService) {
        super();
        this.apiRoot = apiRoot;
        this._start();
        this.stateManager = stateManager;
        this.plcService = plcService;
    }


    _start() {


        this.app = express();
        this.app.use(cors());
        this.app.use(bodyParser.json());


        const router = express.Router();
        this.app.use(router);


        this.server = http.createServer(this.app);

        const port = process.env.PORT || 5000;

        this.server.listen(port, () => console.log(`Listening on port ${port}`));

        const setPlcConnection = ((body, event) => {
            eventBus.emit(event, null, body);
        });

        router.get('/home', (req, res) => {
            // this.stateManager = new StateManager();
            // this.state = this.stateManager.read()
            // this.data = fs.readFileSync(`${__dirname}/store.json`)
            // this.store = JSON.parse(this.data)
            // console.log(this.stateManager.read())
            // console.log('THE_STORE', this.stateManager.read())
            res.send({ home: this.stateManager.read() });
        });

        router.post('/api/openPlcConnection', async (req, res) => {
            // console.log(req.body)
            const {plcIp, plcPort, plcId} = req.body
            // setPlcConnection(body, 'NEW_PLC_CONNECTION')
           
            // eventBus.once('PLC_CONNECTION_CHANGE', (data) => {
            //     res.json(data)
            // });
            try{
            await this.plcService.connect(plcIp, plcPort, plcId)
            }catch(err){
                console.log('PLC_CONNECTION_ERROR', error)
            }
            // console.log('API_RESPONSE', response)
            // this.stateManager = new StateManager();
            // this.state = this.stateManager.read()
            // this.data = fs.readFileSync(`${__dirname}/store.json`)
            // this.store = JSON.parse(this.data)
            // console.log(this.stateManager.read())
            // console.log('THE_STORE', this.stateManager.read())
            ;
        });

        router.post('/api/closePlcConnection', async (req, res) => {
            // console.log(req.body)
            const {plcIp, plcPort, plcId} = req.body
            // const body = req.body
            // setPlcConnection(body, 'CLOSE_PLC_CONNECTION')
            try{
            await this.plcService.disconnect(plcIp, plcPort, plcId)
        }catch(err){
            console.log('PLC_DISCONNECTION_ERROR', error)
        }
            // eventBus.once('PLC_CONNECTION_CHANGE', (data) => res.json(data));
            // console.log('API_RESPONSE', response)
            // this.stateManager = new StateManager();
            // this.state = this.stateManager.read()
            // this.data = fs.readFileSync(`${__dirname}/store.json`)
            // this.store = JSON.parse(this.data)
            // console.log(this.stateManager.read())
            // console.log('THE_STORE', this.stateManager.read())
            ;
        });

        router.get(`${this.apiRoot}`, (req, res) => {
            res.send({ api: '200' });
        });


    }

    getServer() {
        return this.server;
    }
}

module.exports = ApiService;
