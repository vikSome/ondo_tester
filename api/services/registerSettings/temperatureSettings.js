
const TemperatureSettings = [
    {
         bit: 0,
         onMask_bit: 1,
         name: "setting_1"
    // const offMask = 0
    },
    {
        bit: 1,
        onMask_bit: 2,
        name: "setting_2"
    },
    {
       bit: 2,
       onMask_bit: 4,
       name: "setting_3"	
    },
    {
       bit: 3,
       onMask_bit: 8,
       name: "setting_4"	
    },
    {
       bit: 4,
       onMask_bit: 16,
       name: "setting_5"
    }
    ]

    module.exports = TemperatureSettings;
