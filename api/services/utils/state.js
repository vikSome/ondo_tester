const defaultState = {
    temperature: 0,
    humidity: 0,
    BitEncodedTwo: 0,
    BitEncodedOne: 0,
    ventOneTimeOpen: 0,
    ventTwoTimeOpen: 0,
    ventOneOpenTime: 0,
    ventTwoOpenTime: 0,
    plcIp: '',
    plcPort: '',
    plcStatus: 0,
};

module.exports = defaultState;