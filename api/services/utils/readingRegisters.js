const readingRegisters = {
    defaultLength: 1,
    temperature: {
        reg: 1,
        name: 'temperature',
        data: null,
        event: 'TEMPERATURE_CHANGE'
    },
    humidity: {
        reg: 0,
        name: 'humidity',
        data: null,
        event: 'HUMIDITY_CHANGE'
    },
    BitEncodedOne: {
        reg: 3,
        name: 'BitEncodedOne',
        data: null,
        event: 'BIT_ENCODED_ONE_CHANGE'
    },
    BitEncodedTwo: {
        reg: 5,
        name: 'BitEncodedTwo',
        data: null,
        event: 'BIT_ENCODED_TWO_CHANGE'
    },
    ventOneTimeOpen: {
        reg: 10,
        name: 'ventOneTimeOpen',
        data: null,
        event: 'VENT_ONE_TIME_OPEN_CHANGE'
    },
    ventTwoTimeOpen: {
        reg: 15,
        name: 'ventTwoTimeOpen',
        data: null,
        event: 'VENT_TWO_TIME_OPEN_CHANGE'
    },
    ventOneOpenTime: {
        reg: 110,
        name: 'ventOneOpenTime',
        data: null,
        event: 'VENT_ONE_OPEN_TIME_CHANGE'
    },
    ventTwoOpenTime: {
        reg: 115,
        name: 'ventTwoOpenTime',
        data: null,
        event: 'VENT_TWO_OPEN_TIME_CHANGE'
    },
};

module.exports = readingRegisters;