const fs = require('fs').promises;
const eventBus = require('js-event-bus')();
const defaultState = require('./utils/state')

class StateManager {

    constructor() {
        // super()
        // console.log('CONSTRUCTOR_STATE', state)
        this.store = { ...defaultState };

    }

    async init() {
        await this.initStateSubscriptions();
        // await this.readStoreFromStorage();
    }

    read() {
        // console.log("STATE", state)
        return this.store;
    }

    persistStore(store) {
        const storeStringified = JSON.stringify(store, null, 2);
        return fs.writeFile(`${__dirname}/store.json`, storeStringified).then(() => {
            console.log('Saving store', store.ventOneOpenTime);
        }).catch(console.error);
    }

    async readStoreFromStorage() {
        try {
            const store = await fs.readFile(`${__dirname}/store.json`);
            this.store = JSON.parse(store);
        } catch (err) {
            console.error("Unable to read store, initializing empty!")
        }
    }

    saveState(data, name, event) {
        // console.log('DATA', data, 'NAME', name)
        if (data == this.store[name]) {
            return
        } else if (data != this.store[name]) {
            this.store[name] = data;
            console.log(event, this.store[name])
            eventBus.emit(event, null, this.store[name]);
            // this.persistStore(this.store).then(() => console.log('saved humidity'));
        }

        // this.persistStore(this.store)
    }


    writeHumidity(data) {

        if (data == this.store.humidity) {
            return
        } else if (data != this.store.humidity) {
            this.store.humidity = data;
            console.log('HUMIDITY_CHANGE', this.store.humidity)
            eventBus.emit('HUMIDITY_CHANGE', null, this.store.humidity);
            // this.persistStore(this.store).then(() => console.log('saved humidity'));
        }

        // this.persistStore(this.store)
    }

    writeTemp(data) {

        if (data == this.store.temperature) {
            return
        } else if (data != this.store.temperature) {
            this.store.temperature = data
            console.log('TEMPERATURE_CHANGE', this.store.temperature)
            eventBus.emit('TEMPERATURE_CHANGE', null, this.store.temperature)
        }

        // this.persistStore(this.store)
    }

    writeVentOne(data) {

        if (data == this.store.ventOneTimeOpen) {
            return
        } else if (data != this.store.ventOneTimeOpen) {
            // console.log(this.store)
            this.store.ventOneTimeOpen = data
            eventBus.emit('VENT_ONE_TIME_OPEN_CHANGE', null, this.store.ventOneTimeOpen)

        }

        // this.persistStore(this.store)
    }

    writeVentOneOpenTime(data) {

        if (data == this.store.ventOneOpenTime) {
            return
        } else if (data != this.store.ventOneOpenTime) {
            // console.log(this.store)
            this.store.ventOneOpenTime = data
            eventBus.emit('VENT_ONE_OPEN_TIME_CHANGE', null, this.store.ventOneOpenTime)
        }
        console.log('ventOneOpenTime', this.store.ventOneOpenTime);
        // this.persistStore(this.store)
    }


    writeVentTwo(data) {

        if (data == this.store.ventTwoTimeOpen) {
            return
        } else if (data != this.store.ventTwoTimeOpen) {
            // console.log(this.store)
            this.store.ventTwoTimeOpen = data
            eventBus.emit('VENT_TWO_TIME_OPEN_CHANGE', null, this.store.ventTwoTimeOpen)

        }

        // this.persistStore(this.store)
    }

    writeVentTwoOpenTime(data) {

        if (data == this.store.ventTwoOpenTime) {
            return
        } else if (data != this.store.ventTwoOpenTime) {
            // console.log(this.store)
            this.store.ventTwoOpenTime = data
            eventBus.emit('VENT_TWO_OPEN_TIME_CHANGE', null, this.store.ventTwoOpenTime)

        }

        // this.persistStore(this.store)
    }

    writeBitEncodedTwo(data) {

        if (data == this.store.BitEncodedTwo) {
            return
        } else if (data != this.store.BitEncodedTwo) {
            this.store.BitEncodedTwo = data
            eventBus.emit('BIT_ENCODED_TWO_CHANGE', null, this.store.BitEncodedTwo)
        }

        // this.persistStore(this.store)

    }

    writeBitEncodedOne(data) {
        if (data == this.store.BitEncodedOne) {
            return
        } else if (data != this.store.BitEncodedOne) {
            this.store.BitEncodedOne = data
            eventBus.emit('BIT_ENCODED_ONE_CHANGE', null, this.store.BitEncodedOne)
        }

        // this.persistStore(this.store)
    }

    OpenPlcConnection(data) {
        // console.log("PLCIP",data)
        this.store.plcIp = data.plcIp
        this.store.plcPort = data.plcPort
        this.store.plcStatus = data.plcStatus

        eventBus.emit('PLC_CONNECTION_CHANGE_OPEN', null, { 'plcIp': this.store.plcIp, 'plcPort': this.store.plcPort, 'plcStatus': this.store.plcStatus })
    }

    async ClosePlcConnection(data) {
        try {
            this.store = { ...defaultState }
        } catch (err) {
            console.log(err)
        }

        eventBus.emit('PLC_CONNECTION_CHANGE_CLOSE', null, this.store);
    }

    initStateSubscriptions() {
        eventBus.on('HUMIDITY', (data) => this.saveState(parseInt(data.data) / 10, data.name, data.event));
        // eventBus.on('HUMIDITY', (data) => this.writeHumidity(parseInt(data) / 10));
        // eventBus.on('TEMPERATURE', (data) => this.writeTemp(parseInt(data) / 10));
        eventBus.on('TEMPERATURE', (data) => this.saveState(parseInt(data.data) / 10, data.name, data.event));
        // eventBus.on('TEMPERATURE', (data) => console.log('NEW_TEMP_TO_STATE', data.data, data.name));
        // eventBus.on('BIT_ENCODED_ONE', (data) => this.writeBitEncodedOne(parseInt(data)));
        // eventBus.on('BIT_ENCODED_TWO', (data) => this.writeBitEncodedTwo(parseInt(data)));
        // eventBus.on('VENT_ONE_TIME_OPEN', (data) => this.writeVentOne(parseInt(data)));
        // eventBus.on('VENT_TWO_TIME_OPEN', (data) => this.writeVentTwo(parseInt(data)));
        // eventBus.on('VENT_ONE_OPEN_TIME_PERSIST', (data) => this.writeVentOneOpenTime(parseInt(data) / 10));
        // eventBus.on('VENT_TWO_OPEN_TIME_PERSIST', (data) => this.writeVentTwoOpenTime(parseInt(data) / 10));
        // eventBus.on('PLC_CONNECTION_SUCCESS', (data) => this.OpenPlcConnection(data));
        // eventBus.on('PLC_CONNECTION_CLOSED', (data) => this.ClosePlcConnection(data));

        eventBus.on('BIT_ENCODED_ONE', (data) => this.saveState(parseInt(data.data), data.name, data.event));
        eventBus.on('BIT_ENCODED_TWO', (data) => this.saveState(parseInt(data.data), data.name, data.event));
        eventBus.on('VENT_ONE_TIME_OPEN', (data) => this.saveState(parseInt(data.data), data.name, data.event));
        eventBus.on('VENT_TWO_TIME_OPEN', (data) => this.saveState(parseInt(data.data), data.name, data.event));
        eventBus.on('VENT_ONE_OPEN_TIME_PERSIST', (data) => this.saveState(parseInt(data.data) / 10, data.name, data.event));
        eventBus.on('VENT_TWO_OPEN_TIME_PERSIST', (data) => this.saveState(parseInt(data.data) / 10, data.name, data.event));
        eventBus.on('PLC_CONNECTION_SUCCESS', (data) => this.OpenPlcConnection(data));
        eventBus.on('PLC_CONNECTION_CLOSED', (data) => this.ClosePlcConnection(data));
        // eventBus.on('PLC_CONNECTION_SUCCESS', (data) => console.log('PLC_CONNECTION_SUCCESS', data))

    }
}

module.exports = StateManager
