const socketIo = require('socket.io');
const eventEmitter = require('./notification');
const eventBus = require('js-event-bus')();
const fs = require('fs');

const cors = require('cors');

class SocketsService {
    constructor(apiService) {

        this.apiService = apiService;
        this.io = null;
        // this.notificationsBus = notifications;
    }

    start() {
        this.io = socketIo(this.apiService.getServer());

        // let interval;

        this.io.on('toServer', data => {
            console.log(data)
        })

        this.io.on('connection', socket => {
            console.log(`Client UI connected...`);

            // if (interval) {
            //     clearInterval(interval);
            // }

            socket.on('toServer', (data) => {
                // socket.emit("message", msg => {
                //     msg
                // });
                console.log(data)
            })

            //Configuration events
            socket.on('MEASURING_BOX_CONFIG', data => eventBus.emit('MEASURING_BOX_CONFIG', null, data));
            //Manual Mode events from UI
            socket.on('NEW_HUMIDITY_REG', data => eventBus.emit('NEW_HUMIDITY_REG', null, data));
            socket.on('NEW_TEMPERATURE_REG', data => eventBus.emit('NEW_TEMPERATURE_REG', null, data));
            //    socket.on('NEW_TEMPERATURE_REG', data => console.log('NEW_TEMPERATURE_REG',data));

            socket.on('NEW_VENT_ONE_OPEN_TIME', data => eventBus.emit('NEW_VENT_ONE_OPEN_TIME', null, data));
            socket.on('NEW_VENT_TWO_OPEN_TIME', data => eventBus.emit('NEW_VENT_TWO_OPEN_TIME', null, data));

            socket.on('NEW_MAIN_WATERMETER_PULSE_REG', data => eventBus.emit('NEW_MAIN_WATERMETER_PULSE_REG', null, data));
            socket.on('NEW_MIXER_CHANNEL_ONE_PULSE_REG', data => eventBus.emit('NEW_MIXER_CHANNEL_ONE_PULSE_REG', null, data));
            socket.on('NEW_MIXER_CHANNEL_TWO_PULSE_REG', data => eventBus.emit('NEW_MIXER_CHANNEL_TWO_PULSE_REG', null, data));

            socket.on('NEW_PLC_CONNECTION', data => eventBus.emit('NEW_PLC_CONNECTION', null, data));
            socket.on('CLOSE_PLC_CONNECTION', (data) => eventBus.emit('CLOSE_PLC_CONNECTION', null, data));


            //    socket.on('NEW_VENT_ONE_OPEN_TIME', data => console.log('IN_VENT_ONE_OPEN_TIME',data))
            //    socket.on('NEW_VENT_TWO_OPEN_TIME', data => console.log('IN_VENT_TWO_OPEN_TIME',data))
            // socket.on('newTemperatureReg', data => console.log(data))

            //Pass reg data to UI
            eventBus.on('HUMIDITY_CHANGE', (data) => this.send('HUMIDITY_CHANGE', data))
            eventBus.on('TEMPERATURE_CHANGE', (data) => this.send('TEMPERATURE_CHANGE', data))


            eventBus.on('VENT_ONE_TIME_OPEN_CHANGE', (data) => this.send('VENT_ONE_TIME_OPEN_CHANGE', data))
            eventBus.on('VENT_TWO_TIME_OPEN_CHANGE', (data) => this.send('VENT_TWO_TIME_OPEN_CHANGE', data))

            eventBus.on('VENT_ONE_OPEN_TIME_CHANGE', (data) => this.send('VENT_ONE_OPEN_TIME_CHANGE', data))
            eventBus.on('VENT_ONE_OPEN_TIME_CHANGE', (data) => this.send('VENT_ONE_OPEN_TIME_CHANGE', data))

            // eventBus.on('VENT_ONE_OPEN_TIME_CHANGE', (data) => console.log('OUT_VENT_ONE_OPEN_TIME',data))
            // eventBus.on('VENT_ONE_OPEN_TIME_CHANGE', (data) => console.log('OUT_VENT_ONE_OPEN_TIME',data))

            eventBus.on('BIT_ENCODED_ONE_CHANGE', (data) => this.send('BIT_ENCODED_ONE_CHANGE', data))
            eventBus.on('BIT_ENCODED_TWO_CHANGE', (data) => this.send('BIT_ENCODED_TWO_CHANGE', data))

            eventBus.on('PLC_CONNECTION_CHANGE_OPEN', (data) => this.send('PLC_CONNECTION_CHANGE_OPEN', data))
            eventBus.on('PLC_CONNECTION_CHANGE_CLOSE', (data) => this.send('PLC_CONNECTION_CHANGE_CLOSE', data))


            socket.on('disconnect', async () => {
                console.log(`Client UI disconnected...`);
            });
        });

        return this;
    }

    onNotification({ source, data }) {
        this.send(source, data);
    }

    // receive(source,data) {
    //     this.io.on(source, data);
    // }

    send(source, data) {
        this.io.emit(source, data);
    }
}

module.exports = SocketsService;
