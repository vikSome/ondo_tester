const services = {
    ApiService: require('./apiService'),
    SocketsService: require('./socketsService'),
    PLCService: require('./plcService'),
    StateManager: require('./stateManager')
};

module.exports.services = services;
