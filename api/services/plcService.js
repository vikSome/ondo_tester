const ModbusRTU = require('modbus-serial');
const EventEmitter = require('events');
const eventBus = require('js-event-bus')();
const store = require('./utils/state');
const readingRegisters = require('./utils/readingRegisters')

class PLCService extends EventEmitter {
    constructor() {
        super();

        this.initService();
    };

    initService() {
        this.requestTimeout = 100;
        this.io = null;
        this.connectionStatus = false;
        this.timerId = null;
        this.readingRegisters = { ...readingRegisters }

        eventBus.on("MEASURING_BOX_CONFIG", data => this.writeToPlc(100, [data]));
        eventBus.on("NEW_HUMIDITY_REG", data => this.writeToPlc(101, [data * 10]));
        // eventBus.on("NEW_HUMIDITY_REG", data => console.log(data));
        eventBus.on("NEW_TEMPERATURE_REG", data => this.writeToPlc(102, [data * 10]));
        eventBus.on("NEW_VENT_ONE_OPEN_TIME", data => this.writeToPlc(110, [data * 10]));
        // eventBus.on("NEW_VENT_ONE_OPEN_TIME", data => eventBus.emit('VENT_ONE_OPEN_TIME_PERSIST',null, data))
        eventBus.on("NEW_VENT_TWO_OPEN_TIME", data => this.writeToPlc(115, [data * 10]));
        // eventBus.on("NEW_VENT_TWO_OPEN_TIME", data => eventBus.emit('VENT_TWO_OPEN_TIME_PERSIST',null, data))
        // eventBus.on("newTemperatureReg", data => console.log(data))
        eventBus.on("NEW_MAIN_WATERMETER_PULSE_REG", data => this.writeToPlc(120, [data]));
        // eventBus.on("NEW_MAIN_WATERMETER_PULSE_REG", data => console.log('NEW_MAIN_WATERMETER_PULSE_REG', data))
        eventBus.on("NEW_MIXER_CHANNEL_ONE_PULSE_REG", data => this.writeToPlc(121, [data]));
        eventBus.on("NEW_MIXER_CHANNEL_TWO_PULSE_REG", data => this.writeToPlc(122, [data]));
    }

    async connect(plcIP, plcPort, plcID) {

        this.plcID = plcID;
        this.plcPort = plcPort;
        this.plcIP = plcIP;

        try {
            this.client = new ModbusRTU();
            console.log(`Connecting to plc at address ${this.plcIP}:${this.plcPort}`)
            await this.client.connectTCP(this.plcIP, this.plcPort);
            console.log(`Connected successfully`)
            this.connectionStatus = true;
            this.client.setID(this.plcID);
            this.onConnectionSuccess();

            this.clientId  = this.client.setTimeout(this.requestTimeout);

            this.read();

        } catch (err) {
            console.log(err, 'Connection ERROR')
            this.connectionStatus = false
        }
    };

    onConnectionSuccess() {
        eventBus.emit('PLC_CONNECTION_SUCCESS', null, { 'plcIp': this.plcIP, 'plcPort': this.plcPort, 'plcStatus': this.connectionStatus })
    };

    onConnectionClosed() {
        eventBus.emit('PLC_CONNECTION_CLOSED', null, store)
        console.log('PLC connection closed')
    };

    async disconnect() {
        clearTimeout(this.timerId);
    
        try {
            this.client.close();
        }
        catch (err) {
            console.log('Disconnect error: ', err);
        }

        this.timerId = null;
        this.clientId = null;
        this.connectionStatus = false;
        this.plcIP = '';
        this.plcPort = '';
        // console.log('CONNECTION_STATUS', this.connectionStatus)
        this.onConnectionClosed();
        this.client = null;
    };

    async read() {
        // console.log(this)
        if (!this.connectionStatus) {
            return;
        };

        this.timerId = setTimeout(async () => {
            // console.log('Start of PLC READING')

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.humidity.reg, this.readingRegisters.defaultLength);
                // console.log('humidity', data)
                eventBus.emit('HUMIDITY', null, { 'data': data.data, 'name': this.readingRegisters.humidity.name, 'event': this.readingRegisters.humidity.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            };

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.temperature.reg, this.readingRegisters.defaultLength);
                eventBus.emit('TEMPERATURE', null, { 'data': data.data, 'name': this.readingRegisters.temperature.name, 'event': this.readingRegisters.temperature.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            };

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.BitEncodedOne.reg, this.readingRegisters.defaultLength);
                eventBus.emit('BIT_ENCODED_ONE', null, { 'data': data.data, 'name': this.readingRegisters.BitEncodedOne.name, 'event': this.readingRegisters.BitEncodedOne.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            }


            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.BitEncodedTwo.reg, this.readingRegisters.defaultLength);
                eventBus.emit('BIT_ENCODED_TWO', null, { 'data': data.data, 'name': this.readingRegisters.BitEncodedTwo.name, 'event': this.readingRegisters.BitEncodedTwo.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            }

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.ventOneTimeOpen.reg, this.readingRegisters.defaultLength);
                eventBus.emit('VENT_ONE_TIME_OPEN', null, { 'data': data.data, 'name': this.readingRegisters.ventOneTimeOpen.name, 'event': this.readingRegisters.ventOneTimeOpen.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            };

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.ventTwoTimeOpen.reg, this.readingRegisters.defaultLength);
                eventBus.emit('VENT_TWO_TIME_OPEN', null, { 'data': data.data, 'name': this.readingRegisters.ventTwoTimeOpen.name, 'event': this.readingRegisters.ventTwoTimeOpen.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            }

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.ventOneOpenTime.reg, this.readingRegisters.defaultLength);
                eventBus.emit('VENT_ONE_OPEN_TIME_PERSIST', null, { 'data': data.data, 'name': this.readingRegisters.ventOneOpenTime.name, 'event': this.readingRegisters.ventOneOpenTime.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            };

            try {
                const data = await this.client.readHoldingRegisters(this.readingRegisters.ventTwoOpenTime.reg, this.readingRegisters.defaultLength);
                eventBus.emit('VENT_TWO_OPEN_TIME_PERSIST', null, { 'data': data.data, 'name': this.readingRegisters.ventTwoOpenTime.name, 'event': this.readingRegisters.ventTwoOpenTime.event });
            }
            catch (err) {
                console.log('PLC_READ_REG_ERR', err);
            };

            this.read();

        }, 100)
    };

    async writeToPlc(reg, data) {
        if (!this.connectionStatus) {
            return;
        };

        try {
            await this.client.writeRegisters(reg, data);
        }
        catch (err) {
            console.log('Write to Reg Error', err);
        }
    }
}


module.exports = PLCService;